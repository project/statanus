
Installation:
---------------------------
The Statanus module is known to work with Drupal 4.7 and Drupal 5. 

There is no configuration for this module (although that may change in the future; see the accompanying TODO.txt) and it does very little on its own. Most of the configuration is done at a separate uptime monitoring service. See the accompanying README.txt for help on how to best use the Statanus module and for information about free and paid monitoring services.



Drupal 4.7:
---------------------------
To install this module, copy the statanus folder to your preferred modules directory and enable it in "Administer -> Modules"



Drupal 5.0 and higher:
---------------------------
To install this module, copy the statanus folder to your preferred modules directory and enable it in "Administer -> Site building -> Modules"



Uninstalling:
---------------------------
The Statanus module can be uninstalled by first disabling Statanus and deleting it from your server. Statanus does not create any tables in your database.