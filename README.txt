
Description:
---------------------------
The Statanus module adds uptime monitoring support to your site. As it's name suggests, Statanus (the Roman deity that oversees a child's first steps) is designed for fledgling administrators who are advancing their skills and want to monitor their site's uptime. When combined with a monitoring service, this module can help you receive notifications to your email address or mobile phone if your site goes down. (If your mobile phone and your Drupal site both go down at the same time, you may have bigger problems.)

See the accompanying INSTALL.txt for instructions on installing this module.


How it works:
---------------------------
When installed, the Statanus module adds a new page to your site which displays a simple "success" message. This page is displayed at http://example.com/statanus (where "example.com" is the domain name of your site) or http://example.com/?q=statanus (if your site isn't using Clean URLs), and this URL can be checked by a free or paid monitoring service. 

Two free monitoring services you can use with this module are SitePinger and the Open Architecture Community System (OpenACS) Uptime service. 

 http://www.sitepinger.net/
 http://uptime.openacs.org/uptime/

The OpenACS Uptime service, for example, checks your site every 15 minutes for a page containing the "success" message, which is frequent enough for small, low-traffic sites. If for some reason your Drupal site goes down (and the monitoring service can no longer access the "success" message on your site), your monitoring service will notify you by email.



Notes:
---------------------------
It's a good idea to give your monitoring service an email address that lives on a different server or ISP than the one that hosts your Drupal site. If your Drupal site goes down, it's possible that the server or hosting company that handles your email has also been affected. If this is the case, you probably won't receive notification emails.

If you disable this module, be sure to inform your monitoring service or you'll start getting emails (or text messages if you supplied your mobile phone's dedicated email address) notifying you that your site is down when it probably isn't.

The "success" message is only available to a monitoring service if your Drupal site is functioning properly. If you want to check your web server's health independently of your Drupal site's, create a text file that contains the text "success" and put it in a worldly-readable location (such as the root of your Drupal directory). Name it "statanus.txt" or something similar and inform your monitoring service of the text file's URL at http://example.com/statanus.txt (where "example.com" is the domain name of your site and "statanus.txt" is the name of the text document you created).

After installing the Statanus module you may want to add an entry to your robots.txt to disallow search engines from indexing your lonely page with the "success" message.



Author:
---------------------------
Christefano
http://drupal.org/user/76901

This module was developed for Exaltation of Larks and its clients. It is not affiliated or associated with SitePinger or OpenACS.